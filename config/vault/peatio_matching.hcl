path "transit/zigmacoin_*" {
  capabilities = [ "read" ]
}
# Decrypt secrets
path "transit/decrypt/zigmacoin_*" {
  capabilities = [ "create", "update" ]
}
# Use key for signing
path "transit/sign/zigmacoin_*" {
  capabilities = ["update"]
}
# Create transit key
path "transit/keys/zigmacoin_*" {
  capabilities = ["create"]
}
# Renew tokens
path "auth/token/renew" {
  capabilities = ["update"]
}
# Lookup tokens
path "auth/token/lookup" {
  capabilities = ["update"]
}
